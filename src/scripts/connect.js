export default function connect(state, component, filterFn) {
  state.subscribe(
    ()=> component.render(filterFn(state.getState()))
  )

  component.render(filterFn(state.getState()));
}
