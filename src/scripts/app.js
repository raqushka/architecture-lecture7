import reducer, {incrementCounter, loadedRatingData, loadingRatingData} from './reducer';
import store from './store';
import {createStore} from 'redux';
import CounterView from './views/CounterView';
import RaitingView from './views/RaitingView';
import connect from './connect';

;
const counterView = connect(
  store,
  new CounterView(
    document.querySelector('.counter'),
    {incrementCounter: () => store.dispatch(incrementCounter())}
  ),
  (state) => state.counter
);

const raitingView = connect(
  store,
  new RaitingView(document.querySelector('.rating')),
  (state) => state.rating
);

store.dispatch(loadingRatingData());
