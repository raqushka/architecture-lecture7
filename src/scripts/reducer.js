const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const RATING_LOADED = 'RATING_LOADED';
const RATING_LOADING = 'RATING_LOADING';
const arr = [{name: 'aria', points: 650}, {name: 'vova', points:899}, {name:'patrice', points: 800}, {name: 'zagid', points:750}, {name: 'vitya', points: 700}];

export default function reducer(oldState, action) {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return {
        ...oldState,
        counter: oldState.counter + 1,
      }
    case RATING_LOADING:
      return {
        ...oldState,
        rating: {
          entities: [],
          loading: true
        }
      }
    case RATING_LOADED:
      return {
        ...oldState,
        rating: {
          entities: action.data,
          loading: false
        }
      }
    default:
      return oldState;
  }
}

export function incrementCounter() {
  return {
    type: INCREMENT_COUNTER,
  }
}

const promise = new Promise((resolve, reject) => {
  window.setTimeout(function() {
     resolve(arr);
   }, 6000)
})
export function loadingRatingData() {
  return {
    types: [RATING_LOADING, RATING_LOADED],
    promise: promise
  }
}
