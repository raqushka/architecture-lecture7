const middleware = store => next => action => {
 if (action.promise) {
   console.log('action', action)
    const [loading, loaded] = action.types;

    store.dispatch({type: loading});

    return action.promise.then(data => store.dispatch({
      type: loaded,
      data
    }));
 } else {
    return next(action);
  }
}

export default middleware;
