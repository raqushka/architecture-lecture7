import {createStore, applyMiddleware} from 'redux';
import reducer from './reducer';
import logger from './middlewares/logger';
import asyncLoad from './middlewares/async';

const rating = {loading: false, entities: []};
const createStoreWithMiddleware = applyMiddleware(logger, asyncLoad)(createStore);

export default createStoreWithMiddleware(reducer, {counter: 1, rating})
