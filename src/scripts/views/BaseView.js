export default class BaseView {
  constructor(node, action = {}) {
    this._node = node;
    this._action = action;
  }
  render(data) {
    if (!this._rendered) {
      this.initialRender();
    }
  }
}
